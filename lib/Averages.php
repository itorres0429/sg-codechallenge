<?php
class Averages
{
   private $numbers;
   
    # Object constructor
    # Params: (Array) number set to perform math operations on
    public function __construct(array $numbers) {
        $this->numbers = array_filter($numbers, 'is_numeric');
    }
   
    # Retrieves set of numbers
    # Params: None
    # Returns: (Array) set of numbers the object was initialized with
    public function getNumbers() {
        return $this->numbers;
    }
   
    # Calculates mean value of number set
    # Params: None
    # Returns: (Integer) mean value, rounded to 3 decimal places
    public function mean() {
        $count = count($this->numbers);
        $sum = array_sum($this->numbers);     
        return round($sum / $count, 3);
    }
   
    # Calculates median value of number set
    # Params: None
    # Returns: (Integer) median value
    public function median() {
        sort($this->numbers);
        $middle = floor((count($this->numbers)-1) / 2);
        if(count($this->numbers) % 2) {
            $median = $this->numbers[$middle];  
        } else {
            $low = $this->numbers[$middle];
            $high = $this->numbers[$middle + 1];
            $median = (($low + $high) / 2);
        }
        return $median;
    }
   
    # Calculates mode of number set
    # Params: None
    # Returns: (Array) array of mode values
    public function mode() {
        $freq = array_count_values($this->numbers); 
        if(max($freq) == 1) {
            return NULL;
        } else {
            $mode = array_keys($freq, max($freq));
        }
        return $mode;
    }

    # Calculates range of number set
    # Params: None
    # Returns: (Integer) range value
    public function range() {
        return max($this->numbers) - min($this->numbers);
    }
}
?>