=====================================
PHP Code Sample
=====================================

This project implements a very simple api with a single endpoint:

"/mmmr" 
    - Expects an array of numbers called 'numbers' in a POST request
    - Returns a json object of the mean, median, mode, and range of the numbers posted
    - If a value does not exist, an empty string is returned
    - If a request is sent that is not a POST request, a JSON response and a 404 error is returned
    - System errors are handled gracefully with a JSON response and a 500 error


Testing
========

Unit tests were created using phpunit, and should be run by bootstrapping 'autoload.php':

phpunit --bootstrap autoload.php tests
