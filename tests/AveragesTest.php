<?php
class AveragesTest extends PHPUnit_Framework_TestCase {
    
    public function testMean() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 10));
        $this->assertEquals(6, $numSet->mean());
    }
    
    public function testMeanPrecision() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 4, 6));
        $this->assertEquals(5.143, $numSet->mean());
    }

    public function testMedianEvenNumSet() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 4, 29, 16));
        $this->assertEquals(6, $numSet->median());
    }
    
    public function testMedianOddNumSet() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 29, 16));
        $this->assertEquals(7, $numSet->median());
    }

    public function testModeIsArray() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 29, 16));
        $this->assertTrue(is_array($numSet->mode()));
    }

    public function testModeIsNull() {
        $numSet = new Averages(array(3, 5, 7, 4, 29, 16));
        $this->assertNull($numSet->mode());
    }

    public function testModeSingle() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 29, 16));
        $this->assertSame(array(7), $numSet->mode());
    }
    
    public function testModeMultiple() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 3, 29, 16));
        $this->assertCount(2, $numSet->mode());
    }

    public function testRange() {
        $numSet = new Averages(array(3, 5, 7, 7, 4, 3, 29, 16));
        $this->assertEquals(26, $numSet->range());
    }
}
?>