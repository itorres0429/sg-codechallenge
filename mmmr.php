<?php
require_once 'autoload.php';

class requestMethodException extends Exception {};

$json_response = [];

try {

    # Check that request was made via POST
    if ($_SERVER['REQUEST_METHOD'] != 'POST') throw new requestMethodException($_SERVER['REQUEST_METHOD']);
    if (!(isset($_POST['numbers']) && is_array($_POST['numbers']))) throw new Exception;
    
    $obj = new Averages($_POST['numbers']);
    
    # Set response valuess
    $json_response['results']['mean'] = $obj->mean();
    $json_response['results']['median'] = $obj->median();
    $json_response['results']['mode'] = $obj->mode();
    $json_response['results']['range'] = $obj->range();
    
    # Set null values to empty strings
    foreach ($json_response["results"] as $key => $value) {
    if (is_null($value)) {
            $json_response["results"][$key] = "";
        }
    }
}

# Catch exceptions, set response accordingly
catch(requestMethodException $method) {
    $json_response['error']['code'] = 404;
    $json_response['error']['message'] = "Method {$method->getMessage()} not available on this endpoint";
}
catch(Exception $e) {
    $json_response['error']['code'] = 500;
    $json_response['error']['message'] = 'An error occurred. Please check your input.';
}

//Display response as json
header('Content-Type: application/json');
print(json_encode($json_response));
?>